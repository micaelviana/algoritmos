
#include <stdio.h>
#include <time.h>

void shuffle(int *v, int size){
	srand(time(NULL));
	int random, temp;

	for (int i = 0; i < size; ++i){
		random = rand() % size;

		temp = v[i];
		v[i] = v[random];
		v[random] = temp;
	}
}

int main(){
    int v[10] = {1,2,3,4,5,6,7,8,9,10};

    shuffle(v,10);

    for (int i = 0; i < 10; i++){
        printf("%d ",v[i]);
    }
    printf("\n");
    
}
